define SIG3DObject zwj_country
{
   Animation = Area2Animation;
   AnimationType = SIG_ANIMATION_ONESHOT;
   AlphaTest = 0.200000;
   Color = (1.000000,1.000000,1.000000,1.000000);
   Culling = OFF;
   DepthTest = 1;
   Fog = 1;
   Translate = <0,0,0>;
   Rotate = <0,0,0>;
   Scale = <1,1,1>;
   RenderClass = ZBuffered;
   ShadeMode = Smooth;
   Show = true;
   VisTest = true;
   Blend = false;
};
