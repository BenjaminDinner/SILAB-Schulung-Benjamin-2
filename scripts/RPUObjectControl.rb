#---- Object Control ----- # 
# This Script calculates the position of an Object which rotates circle wise around the car,
# so the Object is always in the middle of the sight of view.
class RPUObjectControl < RPU
	def initialize() 
		var_in :@rotation		#rotation of the car
		var_in :@carX		    #x position of the car
		var_in :@carY			#y position of the car
		
		var_out :@objX
		var_out :@objY
		
		var_out :@objZrot

		
	end

	def start()  
		
		@rad = 0.0 
		@sinX = 0.0
		@cosY = 0.0
		
		@testRot = 0.0

		
		return true 
	end

	# Statements entered in the trigger method are executed in each 
	# simulation step. 
	def trigger(t, te) 
		
		# Converts the input into an radiation value 
		# 57.32 is the factor of the rotation value of the car into degree (6.28 equals more or less 360°)
		# (Math::PI)/180 is the factor from degree into radiation
		@rad = (@rotation*57.32)*((Math::PI)/180) 
		
		#Calculates the x and y value of the Object on the circle (Math.cos & Math.sin need radiation values not degree)
		@sinX = Math.cos(@rad)*10
		@cosY = Math.sin(@rad)*10

		#adds the Object position onto the car position / -0.5 is an needed offset idk y?
		@objX = @carX + @sinX 
		@objY = @carY + @cosY -0.5
		
		if @testRot <= 12.56
		@testRot = @testRot + 0.0314
		else
		@testRot = 0.0
		end
		
		@objZrot = @testRot
		
	end
end