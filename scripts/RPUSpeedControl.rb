#---- Speed Control ----- # 
#
#
class RPUSpeedControl < RPU
	def initialize() 
		var_in :@Speed
		
		var_out :@sad
		var_out :@ok
		var_out :@happy
		
		var_out :@txt
		
	end

	def start()  
		
		@counter = 0
		
		return true 
	end

	# Statements entered in the trigger method are executed in each 
	# simulation step. 
	def trigger(t, te) 
		
		@counter = @counter +1
		
		if @Speed <= 120
		@sad = 0
		@ok = 0
		@happy = 1
		elsif @Speed > 120 && @Speed <= 160
		@sad = 0
		@ok = 1
		@happy = 0
		else
		@sad = 1
		@ok = 0
		@happy = 0
		end
		
		if @txt = 0 && @Speed > 160 && @counter % 60 >= 30
		@txt = 1
		elsif @txt = 1 && @Speed > 160 && @counter % 60 < 30
		@txt = 0
		end
		
		
	end
end