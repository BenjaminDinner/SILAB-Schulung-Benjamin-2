package de.uulm.silab.tutorial;

import de.wivw.silab.sys.JPU;

/** Java class 'ObjectControlDPU'.<br>
  *  A test DPU
  * <br>
  * Created: 02.09.2016 (SILABDPUWizard).<br>
  * @author Benjamin Dinner
  * <p>
  * A class that can be loaded as a DPU (using the DPUJava) into SILAB.
  * The {@link #trigger} method will be called
  * periodically while the simulation is running.<br>
  * More callbacks ({@link #prepare}, {@link #start}, {@link #stop} and {@link #release}) are available
  * that are called at respective times during the simulation's lifetime.<p>
  * Communication with other SILAB DPUs is most easily implemented by annotating
  * the fields using the "VarIn", "VarOut" and "VarIO" annotations.
  */
class ObjectControlDPU extends JPU
{
	/** The JPU's constructor.
	  * Every derived JPU class must implement a constructor
	  * that takes one long argument and pass that argument to
	  * the superclass (JPU) constructor.<br>
	  * Within the constructor, the varXXX() methods may be used
	  * to create additional SILAB variables.
	  */
	
	
	
	
	@VarIn(def=0) private double rotation;
	@VarIn(def=0) private double carX;
	@VarIn(def=0) private double carY;
	
	@VarOut(def=0) private double objX;
	@VarOut(def=0) private double objY;
	@VarOut(def=0) private double objZrot;
	
	private double rad;
	private double sinX;
	private double cosY;
	private double testRot;
	
	
	
	public ObjectControlDPU(long peer)
	{
		super(peer);
		rad = 0f;
		sinX = 0f;
		cosY = 0f;
		testRot = 0f;
		
	}

	

	/** Called periodically once the user clicks 'Launch'.<br>
	  * Task: Run preparations that have to be done whenever the
	  * simulation is re-started. The tasks must be subdivided into
	  * small steps (approx. 1 ms). The method will be called repeatedly
	  * as long as JPU_NOTREADY is returned.
	  * @param step Call counter (0 for the first call, 1 for the second, etc.)
	  * @return JPU_READY when the preparations are complete.<br>
	  *         JPU_NOTREADY when more time is needed. The method will be called again.<br>
	  *         JPU_ABORT when an error occurred. The simulation will be stopped.
	  */
	@Override public int start(int step)
	{
		
		return JPU_READY;
	}

	/** Called periodically while the simulation is running.<br>
	  * Task: Implement the JPU's functionality during the simulation.
	  * Each call shouldn't take more than 1 ms to complete.
	  */
	@Override public void trigger(double time, double timeError)
	{
		/** Converts the input into an radiation value 
		  * 57.32 is the factor of the rotation value of the car into degree (6.28 equals more or less 360°)
		  * (Math::PI)/180 is the factor from degree into radiation
		  */
		rad = (rotation * 57.32)*((Math.PI)/180) ;
		
		//Calculates the x and y value of the Object on the circle (Math.cos & Math.sin need radiation values not degree)
		sinX = Math.cos(rad)*10;
		cosY = Math.sin(rad)*10;

		//adds the Object position onto the car position / -0.5 is an needed offset idk y?
		objX = carX + sinX ;
		objY = carY + cosY -0.5;
		
		if (testRot <= 12.56)
		{
			testRot = testRot + 0.0314;
		}
		else
		{
			testRot = 0.0;
		}
		
		objZrot = testRot;
		
	
	}
	
	/** Called periodically once the user clicks 'Stop', or when one
	  * DPU's preparations (during 'Launch') have failed.<br>
	  * Task: Cleanup things before the simulation is stopped. The tasks must be subdivided into
	  * small steps (approx. 1 ms). The method will be called repeatedly
	  * as long as JPU_NOTREADY is returned.
	  * @param step Call counter (0 for the first call, 1 for the second, etc.)
	  * @return JPU_READY when the cleanup is complete.<br>
	  *         JPU_NOTREADY when more time is needed. The method will be called again.<br>
	  *         JPU_ABORT when an error occurred. The simulation will be stopped.
	  */
	@Override public int stop(int step)
	{
		return JPU_READY;
	}

	
	

	
	
}
