package de.uulm.silab.tutorial;

import de.wivw.silab.sys.JPU;

/** Java class 'SpeedControlDPU'.<br>
  *  A test DPU
  * <br>
  * Created: 02.09.2016 (SILABDPUWizard).<br>
  * @author Benjamin Dinner
  * <p>
  * A class that can be loaded as a DPU (using the DPUJava) into SILAB.
  * The {@link #trigger} method will be called
  * periodically while the simulation is running.<br>
  * More callbacks ({@link #prepare}, {@link #start}, {@link #stop} and {@link #release}) are available
  * that are called at respective times during the simulation's lifetime.<p>
  * Communication with other SILAB DPUs is most easily implemented by annotating
  * the fields using the "VarIn", "VarOut" and "VarIO" annotations.
  */
class SpeedControlDPU extends JPU
{
	/** The JPU's constructor.
	  * Every derived JPU class must implement a constructor
	  * that takes one long argument and pass that argument to
	  * the superclass (JPU) constructor.<br>
	  * Within the constructor, the varXXX() methods may be used
	  * to create additional SILAB variables.
	  */
	
	
	
	private int counter;
	
	@VarIn(def=0) private double v;
	@VarOut(def=0) private boolean happy;
	@VarOut(def=0) private boolean ok;
	@VarOut(def=0) private boolean sad;
	@VarOut(def=0) private boolean text;
	
	public SpeedControlDPU(long peer)
	{
		super(peer);
	}

	

	/** Called periodically once the user clicks 'Launch'.<br>
	  * Task: Run preparations that have to be done whenever the
	  * simulation is re-started. The tasks must be subdivided into
	  * small steps (approx. 1 ms). The method will be called repeatedly
	  * as long as JPU_NOTREADY is returned.
	  * @param step Call counter (0 for the first call, 1 for the second, etc.)
	  * @return JPU_READY when the preparations are complete.<br>
	  *         JPU_NOTREADY when more time is needed. The method will be called again.<br>
	  *         JPU_ABORT when an error occurred. The simulation will be stopped.
	  */
	@Override public int start(int step)
	{
		counter = 0;
		return JPU_READY;
	}

	/** Called periodically while the simulation is running.<br>
	  * Task: Implement the JPU's functionality during the simulation.
	  * Each call shouldn't take more than 1 ms to complete.
	  */
	@Override public void trigger(double time, double timeError)
	{
		counter ++;
		
		if(v <= 120)
		{
			sad = false;
			ok = false;
			happy = true;
		}
		else if(v > 120 && v <= 160)
		{
			sad = false;
			ok = true;
			happy = false;	
		}
		else
		{
			sad = true;
			ok = false;
			happy = false;	
		}
		
		if(text == false && v > 160 && counter % 60 >= 30)
		{
			text = true;
		}
		else if(text == true && v > 160 && counter % 60 <= 30)
		{
			text = false;
		}
		
	
	}
	
	/** Called periodically once the user clicks 'Stop', or when one
	  * DPU's preparations (during 'Launch') have failed.<br>
	  * Task: Cleanup things before the simulation is stopped. The tasks must be subdivided into
	  * small steps (approx. 1 ms). The method will be called repeatedly
	  * as long as JPU_NOTREADY is returned.
	  * @param step Call counter (0 for the first call, 1 for the second, etc.)
	  * @return JPU_READY when the cleanup is complete.<br>
	  *         JPU_NOTREADY when more time is needed. The method will be called again.<br>
	  *         JPU_ABORT when an error occurred. The simulation will be stopped.
	  */
	@Override public int stop(int step)
	{
		return JPU_READY;
	}

	
	

	
	
}
