Pool Sec : Full
{
	Executeable = true;
		
	DPUMMI MMI
	{
		Computer = {VIS};
		Index = 500;
		
		WindowX = 0;
		WindowY = 605;
		WindowW = 848;
		WindowH = 480;
		W = 848;
		H = 480;
		
		ImageDisplay Black
		{
			X = 0;
			Y = 0;
			W = 848;
			H = 480;
			Images = ("texture\Background.png");
		};
		
		ImageDisplay Tacho
		{
			X = 25;
			Y = 55;
			W = 370;
			H = 370;
			Images = ("texture\Tacho02.png");
			
			Needle Nadel
			{
				X = 0;
				Y = 0;
				W = 370;
				H = 370;
				XMount = 185;
				YMount = 185;
				XNeedleMount = 9;
				YNeedleMount = 9;
				AngleMin = -27;
				AngleMax = 212;
				ValueMin = 0;
				ValueMax = 260;
				NeedleImage = ("texture\zeigerkl.png");
			};
		};	
		
		ImageDisplay AutomMenu
		{
			X = 690;
			Y = 50;
			W = 110;
			H = 376;
			Images = ("texture\black.png");
			
			ImageDisplay Happy
			{
				X = 4;
				Y = 252;
				W = 102;
				H = 102;
				Images = ("texture\Happy.png");
			};
			ImageDisplay Ok
			{
				X = 4;
				Y = 145;
				W = 102;
				H = 102;
				Images = ("texture\ok.png");
			};
			ImageDisplay Sad
			{
				X = 4;
				Y = 38;
				W = 102;
				H = 102;
				Images = ("texture\Sad.png");
			};
		};
		
		TextDisplay WarningMessagesL1
		{
			X = 405;
			Y = 120;
			W = 290;
			H = 30;
			Alignment = "center";
			Font = "verdana.ttf";
			FontColor = (1.0, 0.0, 0.0);
			Texts = ("Achtung langsamer!");
		};
		
	};


	DPUJava SpeedControlDPU
	{
		Computer = {VIS};
		Index = 501;
		TreeviewGroup = "Tutorial";
		Class = de.uulm.silab.tutorial.SpeedControlDPU;
		v = 0;
	};


	Connections =
	{
		VDyn.v_kmh <-> MMI.Tacho.Nadel.In,
		
		VDyn.v_kmh <-> SpeedControlDPU.v,
		
		SpeedControlDPU.happy <-> MMI.AutomMenu.Happy.Show,
		SpeedControlDPU.ok <-> MMI.AutomMenu.Ok.Show,
		SpeedControlDPU.sad <-> MMI.AutomMenu.Sad.Show,
		SpeedControlDPU.text <-> MMI.WarningMessagesL1.Show
		

		
	};
};





